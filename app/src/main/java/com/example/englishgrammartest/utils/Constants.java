package com.example.englishgrammartest.utils;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public final class Constants {
    public static final DatabaseReference DATABASE_REFERENCE_QUESTION = FirebaseDatabase.getInstance().getReference().child("Questions");
}
