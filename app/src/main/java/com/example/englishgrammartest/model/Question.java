package com.example.englishgrammartest.model;

import java.util.HashMap;
import java.util.Map;

public class Question {
    private String question;
    private Map<String, Boolean> answers = new HashMap<>();

    public Question(String question, Map<String, Boolean> answers) {
        this.question = question;
        this.answers = answers;
    }

    public Question(String question) {
        this.question = question;
    }

    public String getQuestion() {
        return question;
    }

    public Map<String, Boolean> getAnswers() {
        return answers;
    }
}
