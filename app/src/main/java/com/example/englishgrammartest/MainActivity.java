package com.example.englishgrammartest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.example.englishgrammartest.model.Question;
import com.example.englishgrammartest.utils.Constants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.rgb(21, 36, 111)));


        Map<String, Boolean> answers = new HashMap<>();
        answers.put("sdfa", true);
        answers.put("qwqqqq", false);
        Question question = new Question("Question1");
        Constants.DATABASE_REFERENCE_QUESTION.setValue(question);
    }

    public void openBeginnerActivity(View view) {
        Intent intent = new Intent(this, beginner.class);

        startActivity(intent);
    }

    public void openIntermediateActivity(View view) {
        Intent intent = new Intent(this, intermediate.class);

        startActivity(intent);
    }

    public void openAdvancedActivity(View view) {
        Intent intent = new Intent(this, advanced.class);

        startActivity(intent);
    }

    private void setDataQuestion() {
        Map<String, Boolean> answers = new HashMap<>();
        answers.put("sdfa", true);
        answers.put("qwqqqq", false);
        Question question = new Question("Question1", answers);
        Constants.DATABASE_REFERENCE_QUESTION.push().setValue(question);

        Map<String, Boolean> answers3 = new HashMap<>();
        answers3.put("sd123fa", true);
        answers3.put("qw442qqqq", false);
        Question question3 = new Question("Question12", answers3);
        Constants.DATABASE_REFERENCE_QUESTION.push().setValue(question3);

        Map<String, Boolean> answers2 = new HashMap<>();
        answers2.put("sdfddfgfa", true);
        answers2.put("qwqqddaaaaaqq", false);
        Question question2 = new Question("Question13", answers2);
        Constants.DATABASE_REFERENCE_QUESTION.push().setValue(question2);
    }
}
